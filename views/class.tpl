<!DOCTYPE html>
<html>
  <head>
            <script type="text/javascript" src="/static/jquery-3.2.1.min.js"></script>
          <script type="text/javascript" src="/static/jquery-3.2.1.maphlight.min.js"></script>
    <script type="text/javascript" src="/static/Chart.bundle.js"></script>


    <meta charset="UTF-8">
    <title>{{title}}</title>
    <link rel="stylesheet" href="/static/psst.css" type="text/css">
  </head>

  <body>


    <div>
<div class='collection'>
    This is the data for the room from {{start}} to {{end}}
<a id="download">Download as image</a>

<canvas id="chartjs-1" class="chartjs" width="undefined" height="undefined"></canvas><script>
    function getRandomColor() {
    var start = "#AB2131";
    var end = "#6B0C20";
    var min=parseInt(start.replace("#",""), 16);
    var max=parseInt(end.replace("#",""), 16);
    return "#"+Math.floor((Math.random() * (max - min) + min)).toString(16).toUpperCase();
}

new Chart(document.getElementById("chartjs-1"),
{"type":"bar","data":
{"labels":[
    %for cat in dates:
    "{{cat}}",
    %end
],
"datasets":[{"label":"{{ClassName}}",
"data":{{nums}},"fill":false
,"backgroundColor":["rgba(255, 159, 64, 0.2)","rgba(255, 159, 64, 0.2)","rgba(255, 205, 86, 0.2)","rgba(75, 192, 192, 0.2)"],
"borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)","rgb(255, 205, 86)","rgb(75, 192, 192)"],"borderWidth":1}]},"options":{"scales":{"yAxes":[{"ticks":{"beginAtZero":true}}]}}});</script>


    </div>

  </body>

</html>
