<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
            <script type="text/javascript" src="/static/jquery-3.2.1.min.js"></script>
          <script type="text/javascript" src="/static/jquery-3.2.1.maphlight.min.js"></script>

    <meta charset="UTF-8">
    <title>{{title}}</title>
             <link href="/static/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/static/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/static/navbar-fixed-top.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
<!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/home">MQ Counter</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/buildings">Buildings</a></li>
            <li ><a href="#contact">Rooms</a></li>
          </ul>
 <div class="col-sm-3 col-md-3 pull-right">

            <form class="navbar-form" role="search" action="/red" method = "get">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search" name="roomID">
                    <div class="input-group-btn">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </div>
                </div>
            </form>
        </div>

        </div><!--/.nav-collapse -->

      </div>

    </nav>
<div class='container'>

       <div class = 'content'>{{!content}}</div>
<div id="accordion" role="tablist" aria-multiselectable="true" class="pannel-group">
    <div class="card">
    <div class="card-header" role="tab" id="headingOne">
      <h5 class="mb-0">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-controls="collapseOne">
          Ground Floor
        </a>
      </h5>
    </div>

        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
      <div class="card-block">
    <img src="/static/{{picture}}"alt="MQ Map" usemap="#Map" class = "map" width = "100%"/>
    <map name="Map" id="Map">
        % for cord in cordinates:
        <area alt="" title="" href="/room/{{cord[1]}}/{{cord[0]}}" shape="poly" coords="{{cord[3]}}" />
        %end
    </map>
                </div>
    </div>
  </div>
      <div class="card">
    <div class="card-header" role="tab" id="headingTwo">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          1st Floor
        </a>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="card-block">
                <img src="/static/eSixA2.png" alt="" usemap="#Map1" class = "map"/>
    <map name="Map1" id="Map1">
        <area alt="" title="" href="#" shape="poly" coords="308,973,307,1008,349,1023,377,1045,387,1059,551,1059,550,858,399,858,395,971,310,973" />
        <area alt="" title="" href="#" shape="poly" coords="292,967,394,969,392,856,290,855,289,967" />
        <area alt="" title="" href="#" shape="poly" coords="71,632,243,636,243,782,71,779" />
    </map>
                </div>
    </div>
  </div>
</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script type="text/javascript" src="/static/resizer.js"></script>
    <script type="text/javascript" src="/static/jquery-3.2.1.maphlight.min.js"></script>
	<script type="text/javascript">

       $(function () {
            $('.map').maphilight();
        });
		$('map').imageMapResize();

	</script>


    </div>
    <script src="/static/bootstrap.min.js"></script>
  </body>
</html>
