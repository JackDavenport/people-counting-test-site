<!DOCTYPE html>
<html>
  <head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
            <script type="text/javascript" src="/static/jquery-3.2.1.min.js"></script>
          <script type="text/javascript" src="/static/jquery-3.2.1.maphlight.min.js"></script>
    <script type="text/javascript" src="/static/Chart.bundle.js"></script>


    <meta charset="UTF-8">
    <title>{{title}}</title>
       <link href="/static/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/static/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/static/navbar-fixed-top.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
 <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/home">MQ Counter</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="#about">Buildings</a></li>
            <li class="active"><a href="#contact">Rooms</a></li>
          </ul>
 <div class="col-sm-3 col-md-3 pull-right">

            <form class="navbar-form" role="search" action="/red" method = "get">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search" name="roomID">
                    <div class="input-group-btn">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </div>
                </div>
            </form>
        </div>

        </div><!--/.nav-collapse -->

      </div>

    </nav>
<div class='container'>
    <h1 class="display-1">You are in {{title}}</h1>
    <p class = "lead">

    This is the data for {{title}} for the date range {{start}} to {{end}} and the time range {{startT}} to {{endT}}</p>
<form action="/download" method = "post" class="form-inline">
    <input name="building" type="hidden" value="{{Building}}">
        <input name="room" type="hidden" value="{{room}}">
        <input name="dateStart" type="hidden" value="{{start}}">
        <input name="dateEnd" type="hidden" value="{{end}}">
            <input name="timeStart" type="hidden" value="{{startT}}">
        <input name="timeEnd" type="hidden" value="{{endT}}">
     <button type="submit" class="btn btn-primary">Download data as CSV</button>

 <input type="button" class="btn btn-info" value="FoxStream For Room" onclick="location.href = '{{link}}';">

 <input type="button" class="btn btn-info" value="Download graph as image" onclick=";">

<a id="download">Download as image</a>
</form>
    <br>
</select>
    <form action="/room/Class" method = "post"  class = "form-inline">
        <label for="startDate">Start Date</label>
        <div class="form-group">
            <input type="date" class="form-control" id="startDate" value= "{{start}}" name = "bday">
        </div>
        <label for="endDate">End Date</label>
        <div class="form-group">
            <input type="date" class="form-control" id="endDate" value= "{{end}}" name = "bdayEnd">
        </div>
        <label for="startTime">Time Start</label>
         <div class="form-group">
             <select name="startTime" id = "startTime">
                <option value="09" selected>9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
              <option value="20">20</option>
  </select>
              </div>
            <input name="building" type="hidden" value="{{Building}}">
        <label for="endTime">Time End</label>
          <select name="endTime" id = "endTime">
                  <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
                            <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
              <option value="20">20</option>
    <option value="21" selected>21</option>

  </select>
        <input name="room" type="hidden" value="{{room}}">
  <input type="submit">

</form>

    <br>



<canvas id="chartjs-0" class="chartjs" width="undefined" height="undefined">
    <script>
    function getRandomColor() {
    var start = "#AB2131";
    var end = "#6B0C20";
    var min=parseInt(start.replace("#",""), 16);
    var max=parseInt(end.replace("#",""), 16);
    return "#"+Math.floor((Math.random() * (max - min) + min)).toString(16).toUpperCase();
}
 new Chart(document.getElementById("chartjs-0"),
 {"type":"line","data":
 {"labels":{{time}},
    "datasets":[
        %for dan in everything:
     {"label":"{{dan}}","data":{{everything[dan][0]}}, "fill":false,"borderColor":getRandomColor(),"lineTension":0.1},
        %end
 ] }, "options":{

 scales: {
        yAxes: [{
            display: true,
            ticks: {
                suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                // OR //
                beginAtZero: true   // minimum value will be 0.
            }
        }]
    }}}
 );

 /**
 * This is the function that will take care of image extracting and
 * setting proper filename for the download.
 * IMPORTANT: Call it from within a onclick event.
*/
function downloadCanvas(link, canvasId, filename) {
    link.href = document.getElementById(canvasId).toDataURL();
    link.download = filename;
}

/**
 * The event handler for the link's onclick event. We give THIS as a
 * parameter (=the link element), ID of the canvas and a filename.
*/
document.getElementById('download').addEventListener('click', function() {
downloadCanvas(this, 'chartjs-0', 'test.png')
}, false);


</script>
</canvas>

    </div>

  </body>

</html>
