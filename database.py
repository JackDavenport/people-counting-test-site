'''
Created on Mar 26, 2012

@author: steve
'''

import sqlite3
import time
from random import randint, choice


class testCord:
    """
    Provide an interface to the database for a COMP249 web application
    """

    def __init__(self, test=False):
        """
        Construct a database connection.  If test is True, use an
        in-memory database for testing purposes,
        otherwise use a database file called comp249.db
        """

        if test:
            self.dbname = ":memory:"
        else:
            self.dbname = "tempPage.db"

        self.conn = sqlite3.connect(self.dbname)

    def cursor(self):
        """Return a cursor on the database"""

        return self.conn.cursor()

    def commit(self):
        """Commit pending changes"""

        self.conn.commit()
        
    def delete(self):
        """Destroy the database file"""
        pass
        
    def crypt(self, password):
        """Return a one-way hashed version of the password suitable for
        storage in the database"""
        
        import hashlib
        
        return hashlib.sha1(password.encode()).hexdigest()

        # note that this is an insecure hash that should not be used in
        # production. We use it here because it is fast and doesn't slow
        # down testing
        # a better option would be to use pbkdf2_hmac, which runs multiple iterations
        # of the hash function and is intentionally slow. eg.

        #import binascii
        #hash = hashlib.pbkdf2_hmac('sha256', password.encode(), b'randomsalt', 100000)
        #return binascii.hexlify(hash)

        # in python 3.6 we could use hashlib.scrypt
        # see: https://docs.python.org/3/library/hashlib.html#key-derivation




    def create_tables(self):
        """Create and initialise the database tables
        This will have the effect of overwriting any existing
        data."""

        sql = """
         DROP TABLE IF EXISTS SectionCords;
    DROP TABLE IF EXISTS bigMapCords;
    DROP TABLE IF EXISTS rooms;
    DROP TABLE IF EXISTS RoomCords;
     DROP TABLE IF EXISTS RoomInfo;
     	     DROP TABLE IF EXISTS classes;
     CREATE TABLE classes (
               name text,
               Roomname text,
               building text,
              startHour date,
              endHour date,
                            startDate date,
              endDate date,
               PRIMARY KEY (name, startHour, Roomname, building)
     );
    CREATE TABLE rooms (
               name text unique primary key,
               ip text
    );

    CREATE TABLE bigMapCords (
               name text unique primary key,
               link text,
               cord text
    );
     CREATE TABLE SectionCords (
               name text unique primary key,
               sec text,
               link text,
               cord text
    );

         CREATE TABLE RoomCords (
               name text,
               building text,
               link text,
               cord text,
               PRIMARY KEY (name, building)
    );

             CREATE TABLE RoomInfo (
               name text,
               building text,
               hour date,
               num INTEGER,
               PRIMARY KEY (name, building, hour)
    );

    """

        self.conn.executescript(sql)
        self.conn.commit()

    def sample_data(self, random=True):

        self.rooms = [('E7B', '//IP.FOR.E7B'),
                      ('E7A', '//IP.FOR.E7A'),
                      ('E5A', '//IP.FOR.E5A'),
                      ('E5B', '//IP.FOR.E5B'),
                      ('E6A', '//IP.FOR.E6A'),
                      ('E6B', '//IP.FOR.E6B'),
        ]

        cur = self.cursor()
        for post in self.rooms:
            sql = "INSERT INTO rooms (name, ip) VALUES (?, ?)"
            cur.execute(sql, post)

            # commit all updates to the database
        self.commit()

        self.bigMapCords = [('east', '/East', '1379,791,1381,1214,1667,1211,1675,786,1380,794')
            ,('central', '/central', "1035,936,1094,938,1092,1291,1237,1294,1241,1222,1278,1225,1281,946,1330,916,1327,700,1029,728,999,788,1012,912,1038,937")
            ,('west', '/west', "800,941,803,1056,885,1054,904,1079,947,1076,945,1136,1086,1131,1089,944") ]


        cur = self.cursor()
        for post in self.bigMapCords:
            sql = "INSERT INTO bigMapCords (name, link, cord) VALUES (?, ?,?)"
            cur.execute(sql, post)
        self.commit()

        self.classes = [('COMP172', '112', 'E6A','09','2','2017-09-01 09:00:00', '2017-09-22 11:00:00' ),
                        ('COMP172', '112', 'E6A', '10', '2', '2017-09-01 10:00:00', '2017-09-22 11:00:00'),
                        ('COMP172', '102', 'E7A', '09', '2', '2017-09-01 09:00:00', '2017-09-22 11:00:00'),
                        ('COMP172', '121', 'E6A', '10', '2', '2017-09-01 10:00:00', '2017-09-22 11:00:00'),
                      ]
        cur = self.cursor()
        for post in self.classes:
            sql = "INSERT INTO classes (name, Roomname, building, startHour, endHour, startDate, endDate) VALUES (?, ?,?,?,?,?,?)"
            cur.execute(sql, post)
            # commit all updates to the database
        self.commit()

        self.bigMapCords = [('E5A','EAST',  '/building/E5A', '60,167,135,165,136,231,60,230,60,166'),
                            ('E5B', 'EAST', '/building/E5B', '14,233,72,234,71,259,15,259,15,234'),
                            ('E6A', 'EAST', '/building/E6A', '166,169,210,169,212,298,168,300,166,169'),
                            ('E6B', 'EAST', '/building/E6B', '216,168,294,169,294,215,217,215,218,169'),
                            ('E7B', 'EAST', '/building/E7B', '16,15,136,16,136,112,13,106,15,14'),
                            ('E7A', 'EAST', '/building/E7A', '14,146,100,147,102,110,14,106,15,147')
                            ]
        cur = self.cursor()
        for post in self.bigMapCords:
            sql = "INSERT INTO SectionCords (name, sec, link, cord) VALUES (?, ?, ?,?)"
            cur.execute(sql, post)
        self.commit()

        self.RoomInfo = [
            ('102', 'E6A', '2017-09-01 09:00:00', 20),
            ('102', 'E6A', '2017-09-01 10:00:00', 21),
            ('102', 'E6A', '2017-09-01 11:00:00', 19),
            ('102', 'E6A', '2017-09-01 12:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-01 13:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-01 14:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-01 15:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-01 16:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-01 17:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-01 18:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-01 19:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-01 20:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-01 21:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-08 09:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-08 10:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-08 11:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-08 12:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-08 13:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-08 14:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-08 15:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-08 16:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-08 17:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-08 18:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-08 19:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-08 20:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-08 21:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-15 09:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-15 10:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-15 11:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-15 12:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-15 13:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-15 14:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-15 15:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-15 16:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-15 17:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-15 18:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-15 19:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-15 20:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-15 21:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-22 09:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-22 10:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-22 11:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-22 12:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-22 13:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-22 14:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-22 15:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-22 16:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-22 17:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-22 18:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-22 19:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-22 20:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-09-22 21:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-25 09:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-25 10:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-25 11:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-25 12:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-25 13:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-25 14:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-25 15:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-25 16:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-25 17:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-25 18:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-25 19:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-25 20:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-25 21:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-26 09:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-26 10:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-26 11:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-26 12:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-26 13:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-26 14:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-26 15:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-26 16:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-26 17:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-26 18:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-26 19:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-26 20:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-26 21:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-27 09:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-27 10:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-27 11:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-27 12:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-27 13:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-27 14:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-27 15:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-27 16:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-27 17:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-27 18:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-27 19:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-27 20:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-27 21:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-28 09:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-28 10:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-28 11:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-28 12:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-28 13:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-28 14:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-28 15:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-28 16:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-28 17:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-28 18:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-28 19:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-28 20:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-11-28 21:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-25 09:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-25 10:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-25 11:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-25 12:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-25 13:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-25 14:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-25 15:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-25 16:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-25 17:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-25 18:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-25 19:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-25 20:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-25 21:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-26 09:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-26 10:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-26 11:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-26 12:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-26 13:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-26 14:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-26 15:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-26 16:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-26 17:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-26 18:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-26 19:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-26 20:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-26 21:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-27 09:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-27 10:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-27 11:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-27 12:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-27 13:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-27 14:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-27 15:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-27 16:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-27 17:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-27 18:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-27 19:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-27 20:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-27 21:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-28 09:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-28 10:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-28 11:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-28 12:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-28 13:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-28 14:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-28 15:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-28 16:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-28 17:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-28 18:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-28 19:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-28 20:00:00', randint(10, 50)),
            ('102', 'E6A', '2017-12-28 21:00:00', randint(10, 50)),
        ]
        cur = self.cursor()
        for post in self.RoomInfo:
            sql = "INSERT INTO RoomInfo (name, building, hour, num ) VALUES (?, ?,?,?)"
            cur.execute(sql, post)
        self.commit()

        self.RoomCords = [('102', 'E6A', '/room/102', '1380,180,1382,479,1495,594,1725,598,1788,553,1785,501,1752,404,1778,349,1745,257,1665,207,1584,202,1548,209,1482,187,1406,175,1382,178'),
                            ('136', 'E6A', '/building/E5B', '979,716,1145,722,1148,893,980,894,979,719'),
                            ('133', 'E6A', '/building/E6A', '865,901,865,1189,1314,1187,1311,976,1274,973,1272,905,867,904'),
                            ('131', 'E6A', '/building/E6B', '866,1197,865,1447,1268,1449,1270,1373,1312,1365,1311,1197,867,1195'),
                            ('127', 'E6A', '/building/E7B', '869,1457,869,1838,1269,1846,1272,1460,869,1460'),
                          ('121', 'E6A', '/building/E7B', '951,1854,953,2117,1311,2112,1311,2043,1348,2036,1350,1856,952,1852'),
                          ('114', 'E6A', '/building/E7B', '952,2127,953,2389,1315,2388,1311,2315,1350,2310,1349,2123,953,2125'),
                          ('11', 'E6A', '/building/E7B', '954,2395,953,2664,1352,2666,1350,2396'),
                          ('110', 'E6A', '/building/E7B', '1438,3087,1751,3080,1753,2743,1433,2744,1434,3086'),
                          ('109', 'E6A', '/building/E7B', '1434,2393,1437,2734,1757,2735,1758,2397,1434,23914'),
                          ('108', 'E6A', '/building/E7B', '1436,2388,1758,2383,1759,2049,1436,2045,1434,2388'),
                            ('123', 'E6A', '/building/E7A', '14,146,100,147,102,110,14,106,15,147')
                            ]
        cur = self.cursor()
        for post in self.RoomCords:
            sql = "INSERT INTO RoomCords (name, building, link, cord) VALUES (?, ?, ?,?)"
            cur.execute(sql, post)
        self.commit()




if __name__=='__main__':
    # if we call this script directly, create the database and make sample data
    db = testCord()
    db.create_tables()
    db.sample_data()


