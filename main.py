from bottle import Bottle, template, static_file, request, response, HTTPError, redirect
from database import testCord
import datetime
from datetime import timedelta
import shutil
import pyexcel as pe
import os
import copy

db = testCord()
application = Bottle()

@application.route('/')
def index():
    """Overall map
    https://github.com/kemayo/maphilight -> used to highlight things"""
    cursor1 = db.cursor()
    cursor1.execute("""
            SELECT *
          FROM bigMapCords
            """)
    rows = cursor1.fetchall()
    cursor2 = db.cursor()
    cursor2.execute("""
            SELECT *
          FROM rooms
            """)
    rooms = cursor2.fetchall()
    info = {'title': 'Over Map',
            'content': 'start page',
            'picture': 'Map.png',
            'search': rooms,
            'cordinates': rows
            }
    return template('temp', info)


@application.get('/red')
def route():
    room_id = request.query['roomID']
    print(room_id)
    redirect("/building/" + room_id)


@application.route('/East')
def east_side():
    cursor1 = db.cursor()
    cursor1.execute("""
             SELECT *
           FROM SectionCords WHERE sec LIKE 'EAST'
             """)
    rows = cursor1.fetchall()
    for row in rows:
        print(row)
    user = {
        'title': 'East Side Map',
        'content': 'This is the east side',
        'picture': 'east.png',
        'cordinates': rows
    }
    return template('east', user)

@application.route('/loc/<name>')
def user_page(name):
    user = {
        'title': 'Welcome to ' + name,
        'content': 'You are on the ' + name,
        'picture': ''
    }
    return template('temp', user)

@application.route('/building/<name>')
def user_page(name):
    print(name)
    temp = name
    cursorroom = db.cursor()
    sql="""SELECT *
               FROM RoomCords WHERE building LIKE ?
                 """
    cursorroom.execute(sql, (temp,))
    rows = cursorroom.fetchall()
    user = {
        'title': 'Welcome to ' + name,
        'content': 'You are in  ' + name,
        'picture': 'eSixA.png',
        'cordinates': rows
    }
    return template('e6a', user)


@application.route('/room/<building>/<name>')
def user_page(building, name):
    cur = db.cursor()
    sql = "SELECT * FROM RoomInfo WHERE building LIKE ? AND name LIKE ? "
    cur.execute(sql, (building, name,))
    rows = cur.fetchall()
    time = []
    data = []
    d = dict()
    test = cur.fetchone()
    datefirst = rows[0][2][0:10]
    for row in rows:
        if str(row[2][0:10]) > datefirst:
            d[str(datefirst)] = []
            d[str(datefirst)].append(data)
            data = []
            time = []
            datefirst = row[2][0:10]
        time.append(int(row[2][11:13]))  # Take the time instead of the whole datetime
        data.append(row[3])

    d[str(datefirst)] = []
    d[str(datefirst)].append(data)
    curLink = '//link.to.' + name

    l = []
    dict1 = d

    for orange in dict1:
        cat = []
        cat = dict1[orange][0]
        cat.insert(0, orange)
        l.append(cat)
    s2 = pe.Sheet(l)
    filename = building + name + "7.csv"
    s2.save_as('test1.csv')

    for l in d:
        d[l][0].pop(0)

    temp = []
    for key in d:
        temp.append(key)
    cur = db.cursor()
    cur.execute("SELECT * FROM classes WHERE building LIKE ? AND Roomname LIKE ? ", (building, name,))
    clasrow = cur.fetchall()
    for item in clasrow:
        print(item[0] , item[3])

    user = {
        'title': building + name,
        'content': 'The average capacity for today is ',
        'time': time,
        'date': 'dogs',
        'data': data,
        'everything': d,
        'Building' : building ,
        'room':name,
        'start': str(temp[0]),
           'end':str(temp[len(temp)-1]),
        'startT' : 9,
        'endT':21,
        'link': curLink
    }
    return template('room', user)

@application.route('/room/<building>/<name>/<date>')
def user_page(building, name,date):
    print("Ph ello")
    print(date)


@application.route()
def test(building, room):
    print(building)
    print(room)


@application.route('/cheese')
def classes():
    print("Ello")
    cur = db.cursor()
    sql = "SELECT * FROM classes WHERE building LIKE ? AND Roomname LIKE ? "
    cur.execute(sql, ("E6A", "112",))
    rows = cur.fetchall()
    print(rows)
    startDate = datetime.datetime.strptime(rows[0][5], "%Y-%m-%d %H:%M:%S")
    endDate = datetime.datetime.strptime(rows[0][6], "%Y-%m-%d %H:%M:%S")
    curLook = startDate
    l = []
    dur = rows[0][4]
    while curLook < endDate:
        c = []
        l.append(( curLook, curLook + datetime.timedelta(hours=dur)))
        curLook = curLook + datetime.timedelta(days=7)

    averages = []
    print(l)
    dates = []

    for item in l:
        dates.append(item[0])
        cur1 = db.cursor()
        cur1.execute("""
         SELECT AVG(num) FROM RoomInfo WHERE building LIKE ? AND name LIKE ? AND hour BETWEEN ? AND ?
                 """, ("E6A", "112", item[0], item[1]))
        newRow = cur1.fetchall()
        averages.append(newRow[0][0])
    print("DEREK SVAAGE " +str(dates))
    print(averages)
    print(rows[0][0])
    user = {
        'title' : " CHEESE ",
        'start': str(startDate),
        'end':str(endDate),
        'dates':dates,
        'nums':averages,
        'ClassName':rows[0][0],
    }
    return template("class", user)



@application.post('/room/Class')
def pls_work():
    building = request.forms.get('building')
    room = request.forms.get('room')
    dateEnnd = request.forms.get('bdayEnd')

    startTime = request.forms.get('startTime')
    endTime = request.forms.get('endTime')
    date = request.forms.get('bday')   #Cars is the day, and then we need some way to store it so when it goes back to the room screen it remembers
        #Stupid way to do it but cookies.
        #OR we just do the sql look up here,
    #Need to pass list of classes into the original room to have the list dynamically grow
    print(building)
    print(startTime)
    print(endTime)
    start = date + " 09:00:00"
    end = dateEnnd + " 22:00:00"
    print(start)
    print(end)
    cursor1 = db.cursor()
    cursor1.execute("""
    SELECT * FROM RoomInfo WHERE building LIKE ? AND name LIKE ? AND hour BETWEEN ? AND ?
            """,(building, room,start,end))
    rows = cursor1.fetchall()
    time = []
    data = []
    d = dict()
    datefirst = rows[0][2][0:10]
    for row in rows:
        if str(row[2][0:10]) > datefirst:
            d[str(datefirst)] = []
            d[str(datefirst)].append(data)
            data = []
            time = []
            datefirst = row[2][0:10]
        time.append(int(row[2][11:13]))  # Take the time instead of the whole datetime
        data.append(row[3])
    d[str(datefirst)] = []
    d[str(datefirst)].append(data)

    listStart = time.index(int(startTime))
    listEnd = time.index(int(endTime)) + 1
    print(listStart, listEnd)
    temp = []
    print(d)
    for key in d:
        temp.append(key)
        d[key][0]= d[key][0][listStart:listEnd]

    print("CAST " + str(time))
    user = {
        'title': building + room,
        'content': 'The average capacity for today is ',
        'time': time[listStart:listEnd],
        'date': 'dogs',
        'data': data,
        'everything': d,
        'Building' : building ,
        'room':room,
        'start': str(temp[0]),
        'end':str(temp[len(temp)-1]),
        'startT': startTime,
        'endT': endTime,
        'link': "THJINGS"
    }
    return template('room', user)

@application.post('/download')
def download():
    building = request.forms.get('building')
    room = request.forms.get('room')
    dateEnnd = request.forms.get('dateEnd')
    datestart = request.forms.get('dateStart')
    Tstart = request.forms.get('timeStart')
    tEnd = request.forms.get('timeEnd')
    print(building, room, dateEnnd, datestart)
    start = datestart + " " +Tstart+":00:00"
    end = dateEnnd + " " +tEnd+":00:00"
    print(start)
    print(end)
    cursor1 = db.cursor()
    cursor1.execute("""
       SELECT * FROM RoomInfo WHERE building LIKE ? AND name LIKE ? AND hour BETWEEN ? AND ?
               """, (building, room, start, end))
    rows = cursor1.fetchall()
    time = []
    data = []
    d = dict()
    datefirst = rows[0][2][0:10]
    for row in rows:
        if str(row[2][0:10]) > datefirst:
            d[str(datefirst)] = []
            d[str(datefirst)].append(data)
            data = []
            time = []
            datefirst = row[2][0:10]
        time.append(int(row[2][11:13]))  # Take the time instead of the whole datetime
        data.append(row[3])
    d[str(datefirst)] = []
    d[str(datefirst)].append(data)

    l = []
    dict1 = copy.deepcopy(d)
    print(time)
    listStart = time.index(int(Tstart))
    listEnd = time.index(int(tEnd))+1
    for orange in dict1:
        cat = []
        cat = dict1[orange][0]
        newList = cat[listStart:listEnd]
        newList.insert(0, orange)
        print(newList)
        l.append(newList)

    s2 = pe.Sheet(l)
    s2.save_as(str(building+room+Tstart+tEnd)+'.csv')
    print(os.getcwd())
    shutil.move(os.getcwd() +"/"+str(building+room+Tstart+tEnd)+'.csv', os.getcwd() +"/static/"+str(building+room+Tstart+tEnd)+'.csv')
    user = {
        'link': "/"+str(building+room+Tstart+tEnd)+'.csv'
    }
    return template('dw', user)
   # return redirect('/'+str(building+room+dateEnnd+datestart+Tstart+tEnd)+'.csv')
    #print("CAST " + str(d))

@application.route('/rooms/<name>')
def return_room(name):
   # cursor1 = db.cursor()
   # cursor1.execute("""
   #        SELECT *
   #        FROM rooms
   #        WHERE name = ?
   #        """, (name, ))
   # rows = cursor1.fetchall()
   # for row in rows:
    print(name)
   #     redirect(row[1])


@application.route('/static/<picture>')
def serve_pic(picture):
    return static_file(picture, root ='static/')

if __name__ == '__main__':
    application.run(debug=True, port=8010)