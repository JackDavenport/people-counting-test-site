### How do I get set up? ###

To use the prototype python version 3.4+ is required. 
	Install from https://www.python.org/downloads/release/python-362/ and select the appropriate operating system to install python.
Any additional programs used are installed through running this command
```python
pip install -r requirements.txt
```
To run simply open the file main.py by double clicking on it. 

Then in a browser go to http://127.0.0.1:8010/ and the website should appear there.

Currently the only room available is E6A 102.
	Trying to navigate to any other page will result in an error page
	
	To navigate to the room, first select the east side on the home page, then E6A Then room 102 (Its the first one on the top)
	
Once on the rooms page there is some randomly generated test data on display, the data is for some random dates used for testing which highlight how the end system would look